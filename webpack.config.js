var path = require('path');
var webpack = require('webpack');
var devFlagPlugin = new webpack.DefinePlugin({
  __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'false'))
});
var prodFlagPlugin = new webpack.DefinePlugin({
      "process.env": { 
        NODE_ENV: JSON.stringify("production") 
      }
    })

module.exports = {
  entry: path.join(__dirname, 'src/main.js'),
  // entry: [
  //   'webpack-dev-server/client?http://localhost:3000',
  //   'webpack/hot/only-dev-server',
  //   './src/main'
  // ],
  output: {
    path: path.join(__dirname , 'static'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  devServer: {
    inline: true,
    port: 3000
  },
  externals: {
    config: JSON.stringify(require(path.resolve(__dirname, "src/config.json")))
  },
  resolve: {
    modules: [
      path.resolve('./src'),
      path.resolve('./node_modules')
    ]
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.resolve(__dirname, "src"),
        exclude: path.resolve(__dirname, "node_modules"),
        loader: 'babel-loader',
      }
    ]
  },
  plugins:[
    new webpack.HotModuleReplacementPlugin(),
    devFlagPlugin,   
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    prodFlagPlugin
  ]
}
