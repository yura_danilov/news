import React, { Component } from 'react';
import config from 'config';
import { connect } from 'react-redux';
import { newsFetchData } from '../actions/NewsActions';

class NewsList extends Component {
    loadCommentsFromServer(){
        this.props.newsFetchData(config.newsApiUri);
    }

    componentDidMount() {
        this.loadCommentsFromServer();
        this.timer = setInterval(this.loadCommentsFromServer.bind(this), 60000);
    }

    render() {
        if (this.props.hasErrored) {
            return (<p>Sorry! There was an error loading the items</p>);
        }

        if (this.props.isLoading) {
            return (<p>Loading…</p>);
        }

        let newsBox = this.props.news.map((articleData) => {
            let formatedDate = articleData.publishedAt.split('T')[0] + ' ' + articleData.publishedAt.split('T')[1].slice(0, -1);
            
            return (
                <div className="news_box_item">
                    <img src={articleData.urlToImage} />
                    <div className="news_url">
                        <a href={articleData.url} target="_blank">{articleData.title}</a>
                    </div>
                    <span>{articleData.description}</span>
                    <span>{articleData.author}  {formatedDate}</span>
                </div>
            );
        });

        return <div>{newsBox}</div>;
    }
}

const mapStateToProps = (state) => {
    return {
        news: state.news,
        hasErrored: state.newsHasErrored,
        isLoading: state.newsIsLoading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        newsFetchData: (url) => dispatch(newsFetchData(url))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);