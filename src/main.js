import React , { Component } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

import NewsList from './components/NewsList';

const store = configureStore();

render(
    <Provider store={store}>
        <NewsList />
    </Provider>,
    document.getElementById('news')
);