import * as types from '../constants/ActionTypes';
import axios from 'axios';

export function newsFetchDataSuccess(news) {
    return {
        type: types.NEWS_FETCH_SUCCESS,
        news
    };
}

export function newsHasErrored(bool) {
    return {
        type: types.NEWS_FETCH_ERROR,
        hasErrored: bool
    };
}

export function newsIsLoading(bool) {
    return {
        type: types.NEWS_IS_LOADING,
        isLoading: bool       
    };
}

export function newsFetchData(url) {
    return (dispatch) => {
        dispatch(newsIsLoading(true));
 
        axios.get(url)
            .then(response => {
                dispatch(newsIsLoading(false));
                dispatch(newsFetchDataSuccess(response.data.articles));
            })
            .catch(function (error) {
                dispatch(newsHasErrored(true));
            });
    };
}