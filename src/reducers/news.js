import * as types from '../constants/ActionTypes';

export function newsHasErrored (state = false, action) {
    switch (action.type) {
        case types.NEWS_FETCH_ERROR:
            return action.hasErrored;
    
        default:
            return state;
    }
}

export function newsIsLoading (state = false, action) {
    switch (action.type) {
        case types.NEWS_IS_LOADING:
            return action.isLoading;
    
        default:
            return state;
    }
}

export function news (state = [], action) {
    switch (action.type) {
        case types.NEWS_FETCH_SUCCESS:
            return action.news;
    
        default:
            return state;
    }
}