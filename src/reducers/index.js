import { combineReducers } from 'redux';
import { news, newsHasErrored, newsIsLoading } from './news';
 
export default combineReducers({
    news,
    newsHasErrored,
    newsIsLoading
});